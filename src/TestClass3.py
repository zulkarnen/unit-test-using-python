import inspect
import unittest

def add(a, b) -> int :
    print("We're in custom made function : " + inspect.
    stack()[0][3])
    return (a+b)

class TestClass3(unittest.TestCase) :

    def test_case_add_be_true(self) :
        print("\nRunning Test Method : " + inspect.
        stack()[0][3])  
        self.assertEqual(add(5,5), 10)


    def test_case_01(self) :
        print("\nRunning Test Method : " + inspect.
        stack()[0][3])  
        self.assertEqual(add(5,5), 100)

if __name__ == '__main__' :
    unittest.main(verbosity=2)